package ru.csu.server;

public interface StoryRepository {
    void save(Story user);
    void save(String log);
}
